pipeline {

    agent any
    environment{
//we will use the env.variable from jenkins, where we saved our aws credentias
        ACCESS_KEY = credentials('aws_access_key_id')
        SECRET_KEY = credentials('aws_secret_access_key')
    }
    stages {
        stage('git checkout'){
            //this stage will download or clone our git repository into the jenkins workspace.
            steps {
                git branch: 'main', url: 'https://github.com/hesblac/terraform-demo-serverup.git'
            }
        }
        stage('add aws auth'){
            // this stage can be used instead of the env.variables to set our aws cred in the server
            steps{
                sh """
                    aws configure set aws_access_key_id "$ACCESS_KEY"
                    aws configure set aws_secret_access_key "$SECRET_KEY"
                    aws configure set region "us-east-1"
                """
            }
        }
        stage('Terraform init'){
                //this stage initialize terraform for our deployment
            steps {

                script{
                    dir('.'){

                        sh "terraform init"
                    }

                }
            }
        }
        stage('Terraform plan'){
            //the terraform plan part , to show us what we are deploying
            steps {

                dir('.'){

                   sh "terraform plan"
                }

            }
        }
        stage('Terraform approval'){
            //the approval part, which will only ask if the branch pushing or performing the job is a production branch.
            when {branch 'prod'}
            steps {

                script{
                    waitUntil {
                        fileExists('dummyfile')
                    }

                }
            }
        }
        stage('Terraform apply'){
            //this will deploy our terraform code/infrastructure. 
            steps {

                script{
                    dir('.'){

                        sh "terraform apply --auto-approve"
                    }

                }
            }
        }
        stage('Terraform destroy'){
                //this stage will destroy the deployments
            steps {

                script{
                    dir('.'){

                        sh "terraform destroy --auto-approve"
                    }

                }
            }
        }

    }
}
